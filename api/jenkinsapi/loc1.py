import requests
import time
import subprocess

# GitHub repository information
owner = "rahulmrnaico"
repo = "QD"

# API endpoint for the repository's commits
commits_url = f"https://api.github.com/repos/{owner}/{repo}/commits"

# # Path to the directory where the files will be saved
# download_dir = "/path/to/download/directory"

# Last commit SHA1 hash
last_commit_sha = ""

while True:
    # Make a request to the GitHub API to get the latest commit information
    response = requests.get(commits_url)
    response_json = response.json()
    
    # Check if the response has any commits
    if not response_json:
        print("No commits found yet")
        time.sleep(600)
        continue
    
    # Get the SHA1 hash of the latest commit
    latest_commit_sha = response_json[0]["sha"]
    
    # Check if there is a new commit
    if latest_commit_sha != last_commit_sha:
        id=latest_commit_sha[:7]
        print("New commit detected!")
        p=subprocess.Popen('git pull origin main', shell=True, cwd=r"F:\Others\works\Python\QD")
        time.sleep(5)
        p1=subprocess.Popen('git pull --depth=1000', shell=True, cwd=r"F:\Others\works\Python\QD")
        time.sleep(5)
        p2=subprocess.Popen('git reset --hard '+id, shell=True, cwd=r"F:\Others\works\Python\QD")
        time.sleep(5)
        p3=subprocess.Popen("git show -s --format='%an' "+ latest_commit_sha, shell=True, cwd=r"F:\Others\works\Python\QD")
        time.sleep(5)
        p.kill()
        p1.kill()
        p2.kill()
        p3.kill()
        print(latest_commit_sha)
                
        # Update the last commit SHA1 hash
        # print("New commit detected!")
        last_commit_sha = latest_commit_sha
    
    # Wait for some time before checking for updates again
    time.sleep(600)

commit_id="ba7bc8e"



# p=subprocess.Popen('git pull origin main', shell=True, cwd=r"F:\Others\works\Python\QD")
# p=subprocess.Popen('git pull origin main', shell=True, cwd=r"F:\Others\works\Python\QD")
# p=subprocess.Popen('git pull origin main', shell=True, cwd=r"F:\Others\works\Python\QD")
# p=subprocess.Popen('git pull origin main', shell=True, cwd=r"F:\Others\works\Python\QD")